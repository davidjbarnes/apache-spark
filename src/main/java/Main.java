import org.apache.spark.sql.SparkSession;

public class Main {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .config("spark.hadoop.fs.s3a.access.key", System.getenv("AWS_S3_SPARK_KEY"))
                .config("spark.hadoop.fs.s3a.secret.key", System.getenv("AWS_S3_SPARK_SECRET"))
                .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
                .config("spark.hadoop.fs.s3a.fast.upload","true")
                .appName("ExampleSparkApp")
                .getOrCreate();
    }
}