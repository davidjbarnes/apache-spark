# Apache Spark on AWS EC2 with Java

These examples make use of Apache Spark 2.4.0 and Hadoop 2.7.0 on AWS EC2. All examples are in Java. Standalone Apache Spark clustering is used.

## Examples

* [Simple Datasets](https://bitbucket.org/davidjbarnes/apache-spark/src/spark-dataset/)
* [Reading content from AWS S3](https://bitbucket.org/davidjbarnes/apache-spark/src/s3-file-stream/)
* [Kafka Consumer](https://bitbucket.org/davidjbarnes/apache-spark/src/kafka-stream/)
* [Kafka Consumer with Checkpoints on S3](https://bitbucket.org/davidjbarnes/apache-spark/src/kafka-stream-checkpoints/)
* [Twitter Stream](https://bitbucket.org/davidjbarnes/apache-spark/src/twitter-stream/)

## Amazon S3 Considerations

As a result using Spark2.4.0 and Hadoop2.7.0 bundle, you MUST use a legacy version of the AWS Java SDK: aws-java-sdk (1.7.4). Additionally, these jars need to be added to the classpath using the --jars flag; Spark will ensure these dependencies get to needed slaves.

## Future Development

* Run on Yarn cluster
* Run on Mesos cluster
* Process a shit load of data

## Authors

* **David J Barnes** - [Email](NullableInt@gmail.com)

## License

This project is licensed under the MIT License